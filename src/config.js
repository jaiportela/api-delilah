module.exports = {
    PORT: process.env.PORT,
    DB_URI: process.env.MONGODB_URI,
    SALT_ROUNDS: parseInt(process.env.SALT_ROUNDS),
    JWT_PASSWORD: process.env.JWT_PASS,
    REDIS_URL: process.env.REDIS_URL
}