# API Deliah Resto

Esta es la API con el cual el Restaurante DALIAH RESTO, podrá manejar los pedidos, productos y usuarios de su restaurante, teniendo un aplicación con datos persistente con la ayuda de tecnologías como NodeJs, MongoDB, AWS, Nginx, PM2

## Recursos

- Nginx
- PM2
- Node.js
- Express.js
- MongoDB
- Redis
- JWT
- Swagger
- MomentJs
- Bcrypt
- Helmet
- Joi

## Información Antes de Empezar

#### Se debe tener en el computador lo siguiente:

- LLave pem entregada en el archivo

#### Atención

- Para hacer cambios debe de ingresar en la consolo de AWS, ir a EC2, dirigirse a security groups y cambiar el sg que se llama sg_mv_ec2, se debe cambiar que la la regla (inboundrule) de SSH sea para su ip publica.

## Instalación (Para pruebas en el propio computador)

#### 1. Clonar el proyecto

```
git clone https://gitlab.com/jaiportela/api-delilah.git
```

#### 2. Instalar las dependencias

```
npm install
```

#### 3. Correr el servidor

```
npm run start
```

o

```
npm run dev
```

#### 4. Correr el test de registro de usuarios

```
npm test
```

## Dentro de la instancia

Comando para ve los logs de las dos apps corriendo, la api y el test

```
pm2 logs
```

## Documentación

La documentación puede ser accedida desde: [Documentación](https://apidelilah.tk/api-docs)

## A tener en cuenta

La base de datos se llena con unos datos semilla, en donde se llenan los productos, los usuarios por defecto y los metodos de pago.

Los ejemplos en las ordenes se pueden usar para el usuario cliente, para otras pruebas con otros usuarios, los ids de las direcciones se deben de cambiar.

En la sección métodos de pago, el endpoint **get Métodos de Pago** se encuentra abierto tanto para clientes como para los administradores, ya que estos dos son los que deben de tener la posibilidad de ver los métodos pago, no solo los administradores.

## Credenciales

| username | token                                                                                                                                |
| -------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| admin    | eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNjMwNjk5ODY2fQ.oPeebf-K49hCgyds5R0KsJMIxGtPV0tBgWL664SP8ok  |
| client   | eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImNsaWVudCIsImlhdCI6MTYzMDcwMDA3NH0.gbyVnQWlLkIXrv11eQ0eov67gQ6pVTLn9Zd-h_nELV0 |

**\* Autorizarse en Swagger con token**
